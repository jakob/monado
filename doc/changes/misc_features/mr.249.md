math: Expand algebraic math functions in `math/m_api.h`, `math/m_vec3.h` and `math/m_base.cpp`.
