os: Add utility functionality for accessing Bluetooth Low-Energy (Bluetooth LE or BLE) over D-Bus, in `os/os_ble.h` and `os/os_ble_dbus.c`.
