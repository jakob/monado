ci: Perform test builds using the Android NDK (for armeabi-v7a and armv8-a). This is not a full Android port (missing a compositor, etc) but it ensures we don't add more Android porting problems.
