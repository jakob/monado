---
- issue.62
---
Support building with system cJSON instead of bundled copy.
