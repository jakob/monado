psvm: Move the led and rumble updating from the application facing update_inputs
function to the internal thread instead.
