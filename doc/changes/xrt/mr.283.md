Add the new format `XRT_FORMAT_UYVY422` to the interface and various parts of
the code where it is needed to be supported, like the conversion functions and
calibration code. Also rename the `XRT_FORMAT_YUV422` to `XRT_FORMAT_YUYV422`.
