Expose manufacturer and serial number in the prober interface, right now only
for the video device probing. But this is the only thing that really requires
this in order to tell different cameras apart.
