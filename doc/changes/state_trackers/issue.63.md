---
- issue.63
- mr.256
---

Fix compilation issue in `st/gui` when building without OpenCV.
