---
- issue.64
- mr.265
---
Check if `org.bluez` name is available before calling in `os/os_ble_dbus.c`.
