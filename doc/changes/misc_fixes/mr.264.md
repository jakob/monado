Fix leak in `os/os_ble_dbus.c` code when failing to find any device.
