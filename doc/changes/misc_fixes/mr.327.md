CMake build: Rename (nearly) all build options so they begin with `XRT_` and match the defines used in the source. You will probably want to clear your build directory and reconfigure from scratch.
