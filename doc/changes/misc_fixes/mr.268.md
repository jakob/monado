Fix warnings in `util/u_hashset.h` after pedantic warnings were enabled for C++.
