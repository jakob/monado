Implement quad layers.
Add support for multiple projection layers.
Fix bug where OpenGL swap chains were rendered upside down.
